#include "kodi-connection.h"

#include <iostream>

CKodiConnection::CKodiConnection
(
) :
    m_iMessageId(1000000)
{
}// end constructor

CKodiConnection::~CKodiConnection
(
)
{
    CKodiConnection::close();
}// end destructor

void CKodiConnection::connect
(
    const std::string & _szHost,
    ConnectStatusFunction _connectStatusFunction,
    int _iPort
)
{
    // cancel
    m_pCancellable = Gio::Cancellable::create();

    // create a new connection
    m_pClient = Gio::SocketClient::create();

    // connect to the host
    m_pClient->connect_to_host_async(_szHost, _iPort, m_pCancellable,
               sigc::mem_fun(*this, &CKodiConnection::on_connect_to_host)
           );

    m_connectStatusFunction = _connectStatusFunction;
}// end connect

/**
 * Cleans up threads and sockets.
 */
void CKodiConnection::close
(
)
{
    if(m_pCancellable)
    {
        m_pCancellable->cancel();

        // cancel
        m_pCancellable = Gio::Cancellable::create();
    }// end if

    m_pInputstream.reset();

    //TODO: async? Or is that safe on destruct?
    m_pConnection->close();
    m_pConnection.reset();
}// end CKodiConnection::close

void CKodiConnection::command_get_now_playing
(
    ResponseFunction        _responseFunction
)
{
    std::string szRead = "";
    int iId = m_iMessageId ++;

    nlohmann::json j = {
        { "jsonrpc", "2.0" },
        { "method", "Player.GetItem" },
        { "params", {
            { "properties", { "file", "thumbnail" } },
            { "playerid", 1 } } },
        { "id", iId} };

    m_mapResponseIds.insert(std::make_pair(iId, [_responseFunction](const nlohmann::json & _j) {
                auto r = _j.at("result");
                auto i = r.at("item");
                auto l = i.at("label");
                _responseFunction(l.get<std::string>()); // TODO: should this be a glib string?
            }
        ));

    this->command(j.dump());

    // TODO: getting thumbnails, but only http  :(
    //szRead = this->command("{\"jsonrpc\": \"2.0\", \"method\": \"Files.Download\",\"params\": {\"path\": \"image://video@%2fvideo%2fFilms%2fWinged.Migration.1of2.XviD.AC3.MVGroup.org.avi/\"}}");
}// end CKodiConnection::command_get_now_playing

void CKodiConnection::command_rewind
(
)
{
    // TODO: what
}// end CKodiConnection::command_play_pause

void CKodiConnection::command_play_pause
(
)
{
    int iId = m_iMessageId ++;

    nlohmann::json j = {
        { "jsonrpc", "2.0" },
        { "method", "Player.PlayPause" },
        { "params", { { "playerid", 1 } } },
        { "id", iId },
        };

    m_mapResponseIds.insert(std::make_pair(iId, nullptr));

    this->command(j.dump());
}// end CKodiConnection::command_play_pause

void CKodiConnection::command_forward
(
)
{
    // TODO: doesn't work
    int iId = m_iMessageId ++;

    nlohmann::json j = {
        { "jsonrpc", "2.0" },
        { "method", "Player.Seek" },
        { "params", { { "playerid", 1 }, {"value", "smallforward"} } },
        { "id", iId }
        };

    m_mapResponseIds.insert(std::make_pair(iId, nullptr));

    this->command(j.dump());
}// end CKodiConnection::command_play_pause

void CKodiConnection::command_input
(
    const std::string &     _szInputCommand
)
{
    int iId = m_iMessageId ++;

    nlohmann::json j = {
            { "jsonrpc", "2.0" },
            { "method", "Input." + _szInputCommand},
            { "id", iId }
        };

    m_mapResponseIds.insert(std::make_pair(iId, nullptr));

    this->command(j.dump());
}// end CKodiConnection::command_input

void CKodiConnection::command_activate_window
(
    const std::string &     _szWindow,
    const std::string &     _szParameters
)
{
    int iId = m_iMessageId ++;

    nlohmann::json j = {
            { "jsonrpc", "2.0" },
            { "method", "GUI.ActivateWindow"},
            { "id", iId },
            { "params", {
                    { "window", _szWindow }
                } }
        };

    if(_szParameters.length() != 0)
    {
        j["params"]["parameters"] = { _szParameters };
    }// end if

    m_mapResponseIds.insert(std::make_pair(iId, nullptr));

    this->command(j.dump());
}// end CKodiConnection::command_activate_window

void CKodiConnection::command
(
    const std::string &   _szCommand
)
{
    if(m_pConnection && m_pConnection->is_connected())
    {
        // use the connection
        m_pOutputstream->write_async(_szCommand.c_str(), _szCommand.length(),
                sigc::mem_fun(*this, &CKodiConnection::on_write), m_pCancellable
            );
    }// end if
    else
    {
        std::cout << "CKodiConnection::command error - connection not ready to be used" << std::endl;
    }// end else
}// end CKodiConnection::command

/**
 * Parse bytes read and match up brackets to find whole JSON messages.
 */
void CKodiConnection::parse_read
(
)
{
    std::cout << "CKodiConnection::parse_read - read (" + std::string(m_arbyBuffer, m_iSizeRead) << ")" << std::endl;

    for(int i = 0; i < m_iSizeRead; ++ i)
    {
        m_iBracketCount += (m_arbyBuffer[i] == '{');
        m_iBracketCount -= (m_arbyBuffer[i] == '}');

        if(m_iBracketCount == 1 && m_iOpenIndex == -1)
        {
            m_lsszRead.emplace_back();
            m_iOpenIndex = i;
        }// end if
        else if (m_iBracketCount == 0 && m_iOpenIndex != -1)
        {
            m_iCloseIndex = i;
            if(m_iCloseIndex > m_iOpenIndex)
            {
                m_lsszRead.back().append(m_arbyBuffer + m_iOpenIndex, (m_iCloseIndex - m_iOpenIndex) + 1);
                std::cout << "CKodiConnection::parse_read - open (" << m_iOpenIndex << ") close (" << m_iCloseIndex << ")" << std::endl;

                m_iCloseIndex = 0;
                m_iOpenIndex = -1;
            }// end if
        }// end else
    }// end for

    // set ready to read another chunk, trying to find close bracket
    if(m_iOpenIndex != -1)
    {
        m_lsszRead.back().append(m_arbyBuffer + m_iOpenIndex, m_iSizeRead - m_iOpenIndex);
        std::cout << "CKodiConnection::parse_read - append (" << m_iOpenIndex << ") close (" << m_iCloseIndex << ")" << std::endl;
        // reset this, as will need to go from start of next read
        m_iOpenIndex = 0;

        // protect from buffer overflow
        if(m_lsszRead.size() > 2000)
        {
            std::cout << "CKodiConnection::parse_read - read over 2000 characters without close bracket, dumping read data" << std::endl;
            m_lsszRead.clear();
            m_iCloseIndex = 0;
            m_iOpenIndex = -1;
        }// end if
    }// end if

    if(m_iBracketCount == 0)
    {
        try
        {
            for(auto szRead : m_lsszRead)
            {
                auto j = nlohmann::json::parse(szRead);

                if(j.find("id") != j.end() && j.find("result") != j.end())
                {
                    //response(0):{"id":69,"jsonrpc":"2.0","result":"OK"}
                    std::cout << "CKodiConnection::parse_read - response -" << szRead << std::endl;
                    std::map<int, ParseJsonFunction>::iterator it = m_mapResponseIds.find(j["id"]);
                    if(it != m_mapResponseIds.end())
                    {
                        std::cout << "CKodiConnection::parse_read - ACK all okay" << std::endl;
                        if(it->second)
                        {
                            std::cout << "CKodiConnection::parse_read - firing call back" << std::endl;
                            it->second(j);
                        }// end if
                        m_mapResponseIds.erase(it);
                    }// end if
                    else
                    {
                        std::cout << "CKodiConnection::parse_read - warning, response to unknown message" << std::endl;
                    }// end else
                }// end if
                else if(j.find("method") != j.end())
                {
                    //notification(0):{"jsonrpc":"2.0","method":"Player.OnStop","params":{"data":{"end":false,"item":{"title":"","type":"movie"}},"sender":"xbmc"}}
                    std::cout << "CKodiConnection::parse_read - notification " << j["method"] << std::endl;

                    std::string s = j["method"]; // TODO: must be a better way? conversion std > glib string
                    std::map<std::string, EventFunction>::iterator itEvent = m_mapEvents.find(s);
                    if(itEvent != m_mapEvents.end())
                    {
                        itEvent->second(j["params"]);
                    }// end if
                }// end if
            }// end for
        }// end try
        catch(nlohmann::detail::parse_error & e)
        {
            std::cout << "CKodiConnection::parse_read - warning, parsed bad json - " << e.what() << std::endl;
        }// end catch
    }// end if
}// end CKodiConnection::parse_read

void CKodiConnection::on_connect_to_host
(
    Glib::RefPtr<Gio::AsyncResult> & _refAsyncResult
)
{
    try
    {
        m_pConnection = m_pClient->connect_to_host_finish(_refAsyncResult);

        if(m_pConnection)
        {
            m_connectStatusFunction(true);

            m_pInputstream = m_pConnection->get_input_stream();
            m_pOutputstream = m_pConnection->get_output_stream();

            m_pInputstream->read_async  (
                    &m_arbyBuffer, 200,
                    sigc::mem_fun(*this, &CKodiConnection::on_read), m_pCancellable
                );
        }// end if
        else
        {
            std::cout << "CSocketConnectionGlib::on_connect_to_host- connection error - DNS error, service not found, no hosts connectable" << std::endl;
            // TODO: error
            m_connectStatusFunction(false);
        }// end else
    }// end try
    catch(Glib::Error & e)
    {
        std::cout << "CSocketConnectionGlib::on_connect_to_host- connection error: " << e.what() << std::endl;
        // TODO: error
        m_connectStatusFunction(false);
    }// end catch
}// end on_connect_to_host

void CKodiConnection::on_read
(
    Glib::RefPtr<Gio::AsyncResult> & _refAsyncResult
)
{
    try
    {
        // number of bytes read in, or -1 on error, or 0 on end of file.
        m_iSizeRead = m_pInputstream->read_finish(_refAsyncResult);

        if(m_iSizeRead == -1)
            std::cout << "CSocketConnectionGlib::on_read - connection error" << std::endl;
        if(m_iSizeRead == 0)
            std::cout << "CSocketConnectionGlib::on_read - end of stream" << std::endl;
        else
            parse_read();

        // go again!
        m_pInputstream->read_async  (
                    &m_arbyBuffer, 200,
                    sigc::mem_fun(*this, &CKodiConnection::on_read), m_pCancellable
                );
    }// end try
    catch(Glib::Error & e)
    {
        std::cout << "CSocketConnectionGlib::on_read - connection error: " << e.what() << std::endl;
        // TODO: error
    }// end catch
}// end on_read

void CKodiConnection::on_write
(
    Glib::RefPtr<Gio::AsyncResult> & _refAsyncResult
)
{
    try
    {
        int iSizeWrite = m_pOutputstream->write_finish(_refAsyncResult);

        // TODO: hmm
    }// end try
    catch(Glib::Error & e)
    {
        std::cout << "CSocketConnectionGlib::on_write - connection error: " << e.what() << std::endl;
        // TODO: error
    }// end catch
}// end on_write

