#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"

#include "kodi-connection.h"

class CKodiConnectionTest : public CKodiConnection
{
public:
    bool m_bOnAvStartEvent = false;
    bool m_bOnAvChangeEvent = false;

    void testParseRead()
    {
        on_event("Player.OnAVStart", [this](const nlohmann::json & j) {
                m_bOnAvStartEvent = true;
            });
        on_event("Player.OnAVChange", [this](const nlohmann::json & j) {
                m_bOnAvChangeEvent = true;
            });

        parse_read();
    }

protected:
    int read_from_input_stream(char * _pchBuffer, int _size)
    {
        static int i = -1;
        //200
        const char * szTest1 = "({\"jsonrpc\":\"2.0\",\"method\":\"Player.OnAVStart\",\"params\":{\"data\":{\"item\":{\"channeltype\":\"tv\",\"id\":52,\"title\":\"Notts TV\",\"type\":\"channel\"},\"player\":{\"playerid\":1,\"speed\":1}},\"sender\":\"xbmc\"}}{\"jsonrpc\":\"2\"";
        //175
        const char * szTest2 = ".0\",\"method\":\"Player.OnAVChange\",\"params\":{\"data\":{\"item\":{\"channeltype\":\"tv\",\"id\":52,\"title\":\"Notts TV\",\"type\":\"channel\"},\"player\":{\"playerid\":1,\"speed\":1}},\"sender\":\"xbmc\"}}";

        ++ i;

        if(i == 0)
        {
            std::copy(szTest1, szTest1 + 200, _pchBuffer);
            return 200;
        }
        else if(i == 1)
        {
            std::copy(szTest1, szTest2 + 200, _pchBuffer);
            return 175;
        }
        else
        {
            _pchBuffer = "";
            return 0;
        }
    }
};// end CKodiConnectionTest

TEST_CASE( "Test message broken over two reads", "[kodi-connection]" ) {
    CKodiConnectionTest kodiConnectionTest;

    kodiConnectionTest.testParseRead();

    REQUIRE( kodiConnectionTest.m_bOnAvStartEvent == true );
    REQUIRE( kodiConnectionTest.m_bOnAvChangeEvent == true );
}
