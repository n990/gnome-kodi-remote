/* gnome_kodi_remote-window.cpp
 *
 * Copyright 2021 biggaz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gnome_kodi_remote-window.h"
#include <iostream>
#include <gtkmm/messagedialog.h>
#include <glibmm/main.h>

GnomeKodiRemoteWindow::GnomeKodiRemoteWindow()
	: Glib::ObjectBase("GnomeKodiRemoteWindow")
	, Gtk::Window()
	, m_pHeaderBar(nullptr)
	, m_pPreferencesButton(nullptr)
	, m_pRemoteBox(nullptr)
	, m_pPlayingLabel(nullptr)
	, m_pConnectedLabel(nullptr)
	, m_pRewindButton(nullptr)
	, m_pPlayPauseButton(nullptr)
	, m_pForwardButton(nullptr)
	, m_pPropertiesButton(nullptr)
    , m_pInfoButton(nullptr)
    , m_pUpButton(nullptr)
    , m_pDownButton(nullptr)
    , m_pLeftButton(nullptr)
    , m_pRightButton(nullptr)
    , m_pOkayButton(nullptr)
	, m_pBackButton(nullptr)
	, m_pHomeButton(nullptr)
    , m_pFilmsButton(nullptr)
    , m_pTVButton(nullptr)
	, m_pMusicButton(nullptr)
    , m_pPhotoButton(nullptr)
{
	m_builder = Gtk::Builder::create_from_resource("/org/gnome/kodiremote/gnome_kodi_remote-window.ui");
	m_refSettings = Gio::Settings::create("org.gnome.kodiremote");

	m_builder->get_widget("headerbar", m_pHeaderBar);
	m_builder->get_widget("preferences-button", m_pPreferencesButton);
	m_builder->get_widget("remote-box", m_pRemoteBox);
	m_builder->get_widget("playing-label", m_pPlayingLabel);
	m_builder->get_widget("connected-label", m_pConnectedLabel);

	m_builder->get_widget("rewind-button", m_pRewindButton);
	m_builder->get_widget("play-button", m_pPlayPauseButton);
	m_builder->get_widget("forward-button", m_pForwardButton);

	m_builder->get_widget("properties-button", m_pPropertiesButton);
    m_builder->get_widget("info-button", m_pInfoButton);
    m_builder->get_widget("up-button", m_pUpButton);
    m_builder->get_widget("down-button", m_pDownButton);
    m_builder->get_widget("left-button", m_pLeftButton);
    m_builder->get_widget("right-button", m_pRightButton);
    m_builder->get_widget("okay-button", m_pOkayButton);
	m_builder->get_widget("back-button", m_pBackButton);

	m_builder->get_widget("home-button", m_pHomeButton);
    m_builder->get_widget("films-button", m_pFilmsButton);
    m_builder->get_widget("tv-button", m_pTVButton);
	m_builder->get_widget("music-button", m_pMusicButton);
    m_builder->get_widget("photo-button", m_pPhotoButton);

	add(*m_pRemoteBox);
	m_pRemoteBox->show();

	set_titlebar(*m_pHeaderBar);
	m_pHeaderBar->show();

	m_pPreferencesButton->signal_clicked().connect(
	        sigc::mem_fun(*this,
            &GnomeKodiRemoteWindow::onButtonPreferencesClicked)
        );

    // top row
    m_pRewindButton->signal_clicked().connect(
	        sigc::mem_fun(m_kodiConnection,
            &CKodiConnection::command_rewind)
        );

	m_pPlayPauseButton->signal_clicked().connect(
	        sigc::mem_fun(m_kodiConnection,
            &CKodiConnection::command_play_pause)
        );

    m_pForwardButton->signal_clicked().connect(
	        sigc::mem_fun(m_kodiConnection,
            &CKodiConnection::command_forward)
        );

    // middle panel
    m_pInfoButton->signal_clicked().connect(
	        sigc::mem_fun(m_kodiConnection,
            &CKodiConnection::command_info)
        );

    m_pPropertiesButton->signal_clicked().connect(
	        sigc::mem_fun(m_kodiConnection,
            &CKodiConnection::command_context_menu)
        );

    m_pUpButton->signal_clicked().connect(
	        sigc::mem_fun(m_kodiConnection,
            &CKodiConnection::command_up)
        );

    m_pDownButton->signal_clicked().connect(
	        sigc::mem_fun(m_kodiConnection,
            &CKodiConnection::command_down)
        );

    m_pLeftButton->signal_clicked().connect(
	        sigc::mem_fun(m_kodiConnection,
            &CKodiConnection::command_left)
        );

    m_pRightButton->signal_clicked().connect(
	        sigc::mem_fun(m_kodiConnection,
            &CKodiConnection::command_right)
        );

    m_pOkayButton->signal_clicked().connect(
	        sigc::mem_fun(m_kodiConnection,
            &CKodiConnection::command_select)
        );

	m_pBackButton->signal_clicked().connect(
	        sigc::mem_fun(m_kodiConnection,
            &CKodiConnection::command_back)
        );

    // bottom row
    m_pHomeButton->signal_clicked().connect(
	        sigc::mem_fun(m_kodiConnection,
            &CKodiConnection::command_activate_home)
        );

    m_pFilmsButton->signal_clicked().connect(
	        sigc::mem_fun(m_kodiConnection,
            &CKodiConnection::command_activate_films)
        );

    m_pTVButton->signal_clicked().connect(
	        sigc::mem_fun(m_kodiConnection,
            &CKodiConnection::command_activate_tv)
        );

    m_pMusicButton->signal_clicked().connect(
	        sigc::mem_fun(m_kodiConnection,
            &CKodiConnection::command_activate_music)
        );

    m_pPhotoButton->signal_clicked().connect(
	        sigc::mem_fun(m_kodiConnection,
            &CKodiConnection::command_activate_photo)
        );

    // set up preferences dialogue
    m_pDialogPreferences.reset(new Gtk::MessageDialog(*this, "Preferences"));

    m_builder->get_widget("preferences-dialogue", m_pPreferencesBox);
    if(m_pPreferencesBox == nullptr)
        throw std::runtime_error("No \"preferences-dialogue\" object in builder ui file");

    m_builder->get_widget("hostname-entry", m_pEntryHostName);
    m_builder->get_widget("username-entry", m_pEntryUsername);
    m_builder->get_widget("password-entry", m_pEntryPassword);

    m_refSettings->bind("hostname-entry", m_pEntryHostName->property_text());
    m_refSettings->bind("username-entry", m_pEntryUsername->property_text());
    m_refSettings->bind("password-entry", m_pEntryPassword->property_text());

    auto guts = m_pDialogPreferences->get_content_area();
    guts->add(*m_pPreferencesBox);

    m_pDialogPreferences->signal_response().connect(
        sigc::mem_fun(*this, &GnomeKodiRemoteWindow::onPreferencesDone));

    m_kodiConnection.on_event("Player.OnStop", [this](const nlohmann::json & _j) { m_pPlayingLabel->set_text("Nothing playing"); } );
    m_kodiConnection.on_event("Player.OnPause", [this](const nlohmann::json & _j) {  } );
    m_kodiConnection.on_event("Player.OnPlay", [this](const nlohmann::json & _j) {  } );
    m_kodiConnection.on_event("Player.OnAVStart", [this](const nlohmann::json & _j) {
            m_kodiConnection.command_get_now_playing([this](const Glib::ustring & _szResponse) {
                m_pPlayingLabel->set_text(_szResponse.substr(0, 30));
            });
        } );

    auto image = Gtk::make_managed<Gtk::Image>();
    image->set_from_icon_name("go-home", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
    m_pHomeButton->add(*image);
    image->show();

    image = Gtk::make_managed<Gtk::Image>();
    image->set_from_icon_name("video-x-generic", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
    m_pFilmsButton->add(*image);
    image->show();

    image = Gtk::make_managed<Gtk::Image>();
    image->set_from_icon_name("video-display", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
    m_pTVButton->add(*image);
    image->show();

    image = Gtk::make_managed<Gtk::Image>();
    image->set_from_icon_name("audio-x-generic", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
    m_pMusicButton->add(*image);
    image->show();

    image = Gtk::make_managed<Gtk::Image>();
    image->set_from_icon_name("emblem-photos", Gtk::BuiltinIconSize::ICON_SIZE_BUTTON);
    m_pPhotoButton->add(*image);
    image->show();

    // abusing this for first connection
    GnomeKodiRemoteWindow::onPreferencesDone(0);
}// end constructor

void GnomeKodiRemoteWindow::setNowPlaying
(
    const Glib::ustring & _szResponse
)
{
    m_pPlayingLabel->set_text(_szResponse.substr(0, 30));
}// end GnomeKodiRemoteWindow::setNowPlaying

void GnomeKodiRemoteWindow::onButtonPreferencesClicked
(
)
{
    m_pDialogPreferences->show_all();
}// end GnomeKodiRemoteWindow::onButtonPreferencesClicked

void GnomeKodiRemoteWindow::onPreferencesDone
(
    int _iResponseId
)
{
    m_pDialogPreferences->hide();

    // try to connect
    Glib::ustring szHostname = m_refSettings->get_string("hostname-entry");
    m_kodiConnection.connect(szHostname, [szHostname, this](bool _bConnected) {
            if(_bConnected)
            {
                m_pConnectedLabel->set_text("Connected to " + szHostname);

                Glib::signal_idle().connect(sigc::mem_fun(*this, &GnomeKodiRemoteWindow::onIdle));
            }// end if
            else
            {
                m_pConnectedLabel->set_text("Not connected");
                m_pPlayingLabel->set_text("Nothing Playing");

                m_pDialogError.reset(new Gtk::MessageDialog(*this, "Failed to connect to Kodi", true));
                m_pDialogError->set_secondary_text(
                    "Have a look at settings, with the top left button.");

                m_pDialogError->signal_response().connect([this](int _iResponseId){ m_pDialogError->hide(); });

                m_pDialogError->show_all();
            }// end else
        });
}// end GnomeKodiRemoteWindow::onPreferencesDone

bool GnomeKodiRemoteWindow::onIdle
(
)
{
    std::cout << "GnomeKodiRemoteWindow::onIdle - idle" << std::endl;

    m_kodiConnection.command_get_now_playing([this](const Glib::ustring & _szResponse) {
                        m_pPlayingLabel->set_text(_szResponse.substr(0, 30));
                    });

    //  returns false the handler is disconnected
    return false;
}// end GnomeKodiRemoteWindow::onIdle

