#ifndef KODICONNECTION_H
#define KODICONNECTION_H

#include <thread>
#include <list>

#include <giomm/socketconnection.h>
#include <giomm/socketclient.h>
#include <giomm/cancellable.h>

#include "json.hpp"

class CKodiConnection
{
public:
  typedef std::function<void(bool _bConnected)> ConnectStatusFunction;
  typedef std::function<void(const std::string & _szResponse)> ResponseFunction;
  typedef std::function<void(const nlohmann::json & _jParams)> EventFunction;

  CKodiConnection();
  virtual ~CKodiConnection();

  void connect(const std::string & _szHost, ConnectStatusFunction _connectStatusFunction, int _iPort = 9090);
  void close();

  void command_get_now_playing(ResponseFunction _responseFunction = {});

  void command_rewind();
  void command_play_pause();
  void command_forward();

  void command_input(const std::string & _szInputCommand);
  void command_context_menu() { this->command_input("ContextMenu"); }
  void command_info() { this->command_input("info"); }
  void command_up() { this->command_input("Up"); }
  void command_down() { this->command_input("Down"); }
  void command_left() { this->command_input("Left"); }
  void command_right() { this->command_input("Right"); }
  void command_select() { this->command_input("Select"); }
  void command_back() { this->command_input("Back"); }

  void command_activate_window(const std::string & _szWindow, const std::string & _szParameters = "");
  void command_activate_home() { this->command_activate_window("home"); }
  void command_activate_films() { this->command_activate_window("videos", "MovieTitles"); }
  void command_activate_tv() { this->command_activate_window("videos", "TvShowTitles"); }
  void command_activate_music() { this->command_activate_window("music"); }
  void command_activate_photo() { this->command_activate_window("pictures"); }

  void on_event(const std::string & _strEvent, EventFunction _eventFunction) { m_mapEvents[_strEvent] = _eventFunction; }

protected:
  typedef std::function<void(const nlohmann::json & j)> ParseJsonFunction;

  void command(const std::string & _szCommand);
  void parse_read();

  void on_connect_to_host(Glib::RefPtr<Gio::AsyncResult> & _refAsyncResult);
  void on_read(Glib::RefPtr<Gio::AsyncResult> & _refAsyncResult);
  void on_write(Glib::RefPtr<Gio::AsyncResult> & _refAsyncResult);

protected:
  int m_iMessageId;
  char m_arbyBuffer[200];
  int m_iSizeRead;

  int m_iBracketCount = 0;
  int m_iCloseIndex = 0;
  int m_iOpenIndex = -1;
  std::list<std::string> m_lsszRead;

  ConnectStatusFunction m_connectStatusFunction;

  Glib::RefPtr<Gio::SocketClient> m_pClient;
  Glib::RefPtr<Gio::SocketConnection> m_pConnection;
  Glib::RefPtr<Gio::InputStream> m_pInputstream;
  Glib::RefPtr<Gio::OutputStream> m_pOutputstream;
  Glib::RefPtr<Gio::Cancellable> m_pCancellable;

  std::map<int, ParseJsonFunction> m_mapResponseIds;
  std::map<std::string, EventFunction> m_mapEvents;
};// end class CKodiConnection

#endif  // end KODICONNECTION_H
