/* gnome_kodi_remote-window.h
 *
 * Copyright 2021 biggaz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <memory>
#include <gtkmm/builder.h>
#include <gtkmm/headerbar.h>
#include <gtkmm/button.h>
#include <gtkmm/window.h>
#include <gtkmm/box.h>
#include <gtkmm/label.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/entry.h>
#include <giomm/settings.h>

#include "kodi-connection.h"

class GnomeKodiRemoteWindow : public Gtk::Window
{
public:
	GnomeKodiRemoteWindow();

  void setNowPlaying(const Glib::ustring & _szResponse);
  void onButtonPreferencesClicked();
  void onPreferencesDone(int _iResponseId);
  bool onIdle();

private:
  std::unique_ptr<Gtk::MessageDialog> m_pDialogPreferences;
  std::unique_ptr<Gtk::MessageDialog> m_pDialogError;

	Gtk::HeaderBar * m_pHeaderBar;
  Gtk::Button * m_pPreferencesButton;
  Gtk::Box * m_pRemoteBox;
  Gtk::Label * m_pPlayingLabel;
  Gtk::Label * m_pConnectedLabel;

  Gtk::Button * m_pRewindButton;
  Gtk::Button * m_pPlayPauseButton;
  Gtk::Button * m_pForwardButton;

  Gtk::Button * m_pPropertiesButton;
  Gtk::Button * m_pInfoButton;
  Gtk::Button * m_pUpButton;
  Gtk::Button * m_pDownButton;
  Gtk::Button * m_pLeftButton;
  Gtk::Button * m_pRightButton;
  Gtk::Button * m_pOkayButton;
	Gtk::Button * m_pBackButton;

  Gtk::Button * m_pHomeButton;
  Gtk::Button * m_pFilmsButton;
  Gtk::Button * m_pTVButton;
	Gtk::Button * m_pMusicButton;
  Gtk::Button * m_pPhotoButton;

  Gtk::Box * m_pPreferencesBox;

  Gtk::Entry * m_pEntryHostName;
  Gtk::Entry * m_pEntryUsername;
  Gtk::Entry * m_pEntryPassword;

	Glib::RefPtr<Gtk::Builder> m_builder;
  Glib::RefPtr<Gio::Settings> m_refSettings;

  CKodiConnection m_kodiConnection;
};
